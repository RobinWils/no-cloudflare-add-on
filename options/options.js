/**
 * No Cloudflare - A browser add-on to block Cloudflare requests.
 * *
 * Copyright (C) 2019 Robin Wils
 *
 * This file is part of No Cloudflare.
 *
 * No Cloudflare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * No Cloudflare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with No Cloudflare.  If not, see <http://www.gnu.org/licenses/>.
 */


function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
        'noCloudflareIsEnabled': document.querySelector(
            "#enable-no-cloudflare-checkbox"
        ).checked,
        'whitelist': document.querySelector(
            "#whitelist"
        ).value
        'blacklist': document.querySelector(
            "#blacklist"
        ).value
    });
}

function restoreOptions() {

    function setCurrentChoice(result) {
        
        'noCloudflareIsEnabled': document.querySelector(
            "#enable-no-cloudflare-checkbox"
        ).checked = result.noCloudflareIsEnabled;
        
        'whitelist': document.querySelector(
            "#whitelist"
        ).value = result.whitelist;
        
        'blacklist': document.querySelector(
            "#blacklist"
        ).value = result.blacklist;
        
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    var getting = browser.storage.local.get();
    getting.then(setCurrentChoice, onError);
}

document.addEventListener(
    "DOMContentLoaded",
    restoreOptions
);

document.querySelector(
    "form"
).addEventListener(
    "submit",
    saveOptions
);
