<!--
No Cloudflare is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

No Cloudflare is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with No Cloudflare.  If not, see <https://www.gnu.org/licenses/>.
-->

# No Cloudflare add-on
No Cloudflare aims to address the Cloudflare problem. It is a free add-on.
It blocks requests from Cloudflare.

## Project status
Discontinued - I discovered the [Cloud Firewall](https://gitlab.com/gkrishnaks/cloud-firewall) add-on and decided to discontinue this project. I might instead contribute to that add-on.

![.screenshots/popup-screenshot1.png](https://gitlab.com/RobinWils/no-cloudflare-add-on/raw/master/.screenshots/popup-screenshot1.png)

## Install the development version
Mozilla Firefox (and browsers based on it) is the first targeted platform.
Other browsers aren't supported yet. 

0. Clone and unzip this git repository
1. Go to `about:debugging`
2. Enable add-on debugging
3. Click on `Load Temporary add-on`
4. Select `background.js` from this project

## TODO list
- Find a way to convert URLs to IPs (background.js - usesCloudflare method)
- Fix weird checkbox behavior (popup/popup.js)
- Implement black- and whitelist (popup/popup.js)
- Fix options page (options/options.js)

## License
### GPLv3
This project is **free software** and is **licensed under the GPLv3** license.  
Contributions are welcome.
