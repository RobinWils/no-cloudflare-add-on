/**
 * No Cloudflare - A browser add-on to block Cloudflare requests.
 * *
 * Copyright (C) 2019 Robin Wils
 *
 * This file is part of No Cloudflare.
 *
 * No Cloudflare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * No Cloudflare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with No Cloudflare.  If not, see <http://www.gnu.org/licenses/>.
 */


// Enable or disable No Cloudflare
function enableCloudflareBlocking(storedSettings) {
    if (storedSettings.noCloudflareIsEnabled) {
        // Enable 
        browser.webRequest.onBeforeRequest.addListener(
            blockCloudflare,
            {urls: ["<all_urls>"]},
            ["blocking"]
        );
    }
    else {
        // Disable
        browser.webRequest.onBeforeRequest.removeListener(
            blockCloudflare
        );
    }
}


function usesCloudflare(request) {
    /* Should get the IP from a URL, 
       and should compare it with the Cloudflare IPs */
    return true;
}


// Block the Cloudflare IPs
function blockCloudflare(request) {
    let result = {};
    
    // Block the request if
    
    // It is not on the whitelist
    // and if it is on the blacklist
    
    // It is not on the whitelist
    // and uses Cloudflare
    
    // It is not on the whitelist
    // and uses Cloudflare and it is on the blacklist
    
    if(!request.url in storedSettings.whitelist &&
       (request.url in storedSettings.blacklist || usesCloudflare(request))
      ) {
        result.cancel = true;
    }
    return result;
}


// Get the current settings
browser.storage.local.get().then((storedSettings) => {
    
    // Store the default settings if we don't have local settings yet
    if (!storedSettings.noCloudflareIsEnabled ||
        !storedSettings.whitelist ||
        !storedSettings.blacklist) {
        
        browser.storage.local.set({
            'noCloudflareIsEnabled': true,
            'whitelist': [],
            'blacklist': []
        }); 
    }

    // Enable or disable Cloudflare blocking
    enableCloudflareBlocking(storedSettings);
    
}).catch(()=> {
    console.log(
        "Error retrieving stored settings"
    );
});


// Update No Cloudflare status whenever stored settings change
browser.storage.onChanged.addListener((newSettings) => {
    
    enableCloudflareBlocking(newSettings);
    
});
