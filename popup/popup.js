/**
 * No Cloudflare - A browser add-on to block Cloudflare requests.
 * *
 * Copyright (C) 2019 Robin Wils
 *
 * This file is part of No Cloudflare.
 *
 * No Cloudflare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * No Cloudflare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with No Cloudflare.  If not, see <http://www.gnu.org/licenses/>.
 */


// Update the UI
function updateUI(storedSettings) {
    
    // Update the checkbox status
    document.getElementById(
        "enable-no-cloudflare-checkbox"
    ).checked = storedSettings.noCloudflareIsEnabled;

    
    // Change the opacity of the noCloudflareIcon
    let noCloudflareIcon = document.getElementById(
        "no-cloudflare-icon"
    );

    
    if (storedSettings.noCloudflareIsEnabled) {
        noCloudflareIcon.style.opacity = 1; 
    }
    
    else {
        noCloudflareIcon.style.opacity = .2;
    }
    
}


// Get the current settings
browser.storage.local.get().then((storedSettings) => {

    // Update the UI
    updateUI(storedSettings);
    
}).catch(()=> {
    console.log(
        "Error retrieving stored settings"
    );
});

// Update No Cloudflare status whenever stored settings change
browser.storage.onChanged.addListener((newSettings) => {

    // Update the UI
    updateUI(newSettings);
    
});


// TODO: Fix weird checkbox behavior
// On change checkbox events
document.addEventListener("change", (event) => {
    
    // On checkbox change
    if (event.target.id == "enable-no-cloudflare-checkbox") {
        
        // Set variables
        let noCloudflareIsEnabled = document.getElementById(
            "enable-no-cloudflare-checkbox"
        ).checked;
        
        // Change the settings
        browser.storage.local.set({
            'noCloudflareIsEnabled': noCloudflareIsEnabled
        });
        
    }
    
});
